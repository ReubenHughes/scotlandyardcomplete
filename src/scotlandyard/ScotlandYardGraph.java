package scotlandyard;

import graph.*;

import java.util.*;

public class ScotlandYardGraph extends UndirectedGraph<Integer, Transport> {

    /**
     * This method returns a list of Move objects which are possible from a given players location.
     * (This doesn't consider if a move is valid)
     *
     * @param player The colour of the player whose moves we want to generate
     * @param location The integer representing the node where the player is located
     * @return a list of all possible moves from the node to another node
     */
    public List<Move> generateMoves(Colour player, Integer location)
    {
      List<Move> moves = new ArrayList<Move>();
      MoveTicket moveTicket;
      List<Edge<Integer, Transport>> allEdges = this.getEdgesFrom(this.getNode(location));
      if (allEdges != null)
      {
        for (Edge<Integer, Transport> move: allEdges)
        {
          moveTicket = MoveTicket.instance(player, Ticket.fromTransport(move.getData()),move.getTarget().getIndex());
          moves.add(moveTicket);
        }
      }
      return moves;
    }
}
