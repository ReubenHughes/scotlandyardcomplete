package scotlandyard;

import java.util.List;

public class PoliceData implements User {
  private Player player;
  private Integer location;
  private Colour colour;

  /**
   * Constructs a new PlayerData object.
   *
   * @param player the Player object associated with the player.
   * @param colour the colour of the player.
   * @param location the location of the player.
   */
  public PoliceData(Player player, Integer location, Colour colour)
  {
    this.player = player;
    this.location = location;
    this.colour = colour;
  }

    /**
     * Returns the Colour object associated with the player.
     *
     * @return the Colour object associated with the player.
     */
  public Colour getColour() { return colour; }


    /**
     * Returns the Player object associated with the player.
     *
     * @return the Player object associated with the player.
     */
  public Player getPlayer() { return player; }


    /**
     * Returns the player's current tickets.
     *
     * @return the player's current tickets.
     */
  public Integer getLocation() { return location; }

    /**
     * Sets the player's current location.
     *
     * @param location the player's current location.
     */
  public void setLocation(Integer location) { this.location = location; }

    /**
     * Returns a list of valid moves for a police player
     *
     * @param graph The scotland yard graph of nodes that the game is played on
     * @return a list of moves that the police player can make
     */
  public List<Move> getValidMoves(ScotlandYardGraph graph) { return graph.generateMoves(colour, getLocation()); }

    /**
     * A method which checks how many of a given ticket a player has
     *
     * @param ticket The type of ticket to check
     * @return the number of the given ticket that the player has
     */
  public Integer getNumberOfTickets(Ticket ticket) { return 0; }

    /**
     * Plays a given move for the police player in question
     */
  public void play(Move move) { setLocation(((MoveTicket) move).target); }

}
